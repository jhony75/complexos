require 'tty-prompt'

require_relative 'operations.rb'
require_relative 'export.rb'


@imaginary = Operations.new
@prompt = TTY::Prompt.new
@export = Export.new

def escolha_opcao
  print "\nInsira o número da opção desejada: "
  escolha = gets.chomp.to_i
end

def escolha_operacao
  entrada_user = @prompt.select("Escolha uma opção: ", {'Encerrar' => 0, 'Soma' => 1, 'Subtração' => 2, 'Multiplicação' => 3, 'Divisão' => 4, 'Escrever código LaTeX' => 5}, help:"(Use as setas do teclado para se movimentar)")
end

def index

  case escolha_operacao
  when 0
    @export.compile
    return 0
  when 1
    print 'Identificação do Exercício: '
    ex_id = gets.chomp
    print 'Z = '
    z = Complex(gets.chomp)
    print 'W = '
    w = Complex(gets.chomp)
    @imaginary.adition(z, w)
    @export.plus(z, w, ex_id)
    index
  when 2
    print 'Identificação do Exercício: '
    ex_id = gets.chomp
    print 'Z = '
    z = Complex(gets.chomp)
    print 'W = '
    w = Complex(gets.chomp)
    @imaginary.subtraction(z, w)
    @export.minus(z, w, ex_id)
    index
  when 3
    print 'Identificação do Exercício: '
    ex_id = gets.chomp
    print 'Z = '
    z = Complex(gets.chomp)
    print 'W = '
    w = Complex(gets.chomp)
    @imaginary.multiplication(z, w)
    @export.multiply(z, w, ex_id)
    index
  when 4
    print 'Identificação do Exercício: '
    ex_id = gets.chomp
    print 'Z = '
    z = Complex(gets.chomp)
    print 'W = '
    w = Complex(gets.chomp)
    @imaginary.division(z, w)
    @export.divide(z, w, ex_id)
    index
  when 5
    @export.writeLatex
  else
    puts 'Opção invalida, o programa será encerrado.'
    return 0
  end
end

index