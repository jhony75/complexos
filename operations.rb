# TODO, Consertar a função de divisão.

class Operations

  def adition(z, w)
    # z + w = (a + bi) + (c + di) = (a + c) + (b + d)i
    puts "#{z} + #{w} ="
    puts"( #{z.real} + #{z.imaginary}i ) + ( #{w.real} + #{w.imaginary}i ) = "
    puts "( #{z.real} + #{w.real} ) + ( #{z.imaginary} + #{w.imaginary} )i = "
    puts "Resultado = #{z.real + w.real} + #{z.imaginary + w.imaginary}i\n\n"
  end

  def subtraction(z, w)
    # z - w = (a + bi) - (c + di) = (a - c) + (b - d)i
    puts "#{z} - #{w} ="
    puts "( #{z.real} + #{z.imaginary}i ) - ( #{w.real} + #{w.imaginary}i ) = "
    puts "( #{z.real} - #{w.real} ) + ( #{z.imaginary} - #{w.imaginary} )i = "
    puts "Resultado = #{z.real - w.real} + #{z.imaginary - w.imaginary}i\n\n"
  end

  def multiplication(z, w)
    # z * w = (a + bi) * (c + di) = (ac -bd) + (ad + bc)i
    puts "#{z} * #{w} = "
    puts "( #{z.real} + #{z.imaginary} ) * #{w.real} + #{w.imaginary} ) = "
    puts "#{z.real} * #{w.real} + #{z.real} * #{w.imaginary}i + #{w.real} * #{z.imaginary} + #{z.imaginary} * #{w.imaginary}i ^ 2"
    puts "Resultado = #{Complex(z.real, z.imaginary) * Complex(w.real, w.imaginary)}\n\n"
  end

  def division(z, w)
    # Escrever Documentação em PDF
    # puts "#{z} / #{w} = "
    # puts "( #{z} * #{Complex(w).conjugate}) / (#{w} * #{Complex(w).conjugate} ) = " #Fração
    # puts "( (#{z} / #{w}) * (#{Complex(w).conjugate} / #{Complex(w).conjugate}) )"
    # puts "a * c = ( #{z.real} * #{w.real}  ) = #{z.real * w.real} "
    # puts "a * di = ( #{z.real} * #{-w.imaginary}i  ) = #{z.real * Complex(0, w.imaginary).conjugate}"
    # puts "bi * c = ( #{z.imaginary}i * #{w.real} ) = #{z.imaginary * w.real}"
    # puts "bi * di = ( #{z.imaginary}i * #{w.imaginary} ) = #{Complex(z.imaginary * w.imaginary)}"
    puts "#{Complex(z)} / #{Complex(w)} = #{Complex(z / w)}\n\n"
  end
end