# file = File.open('work.tex', 'w') -> Ruby standard

require 'tty-file'

class Export
  def plus(z, w, ex_id)
    TTY::File.append_to_file('Export/Trabalho.tex') do
      "\n\n\\textbf{#{ex_id})} $ #{z} + #{w} = $
      \n$ ( #{z.real} + #{z.imaginary}i ) + ( #{w.real} + #{w.imaginary}i ) = $
      \n$ ( #{z.real} + #{w.real} ) + ( #{z.imaginary} + #{w.imaginary} )i $
      \n$ \\textbf{Resultado = } #{z.real + w.real} + #{z.imaginary + w.imaginary}i $
      \n \\bigskip"
    end
  end

  def minus(z, w, ex_id)
    TTY::File.append_to_file('Export/Trabalho.tex') do
      "\n\n\\textbf{#{ex_id})} $ #{z} - #{w} = $
      \n$ ( #{z.real} + #{z.imaginary}i ) - ( #{w.real} + #{w.imaginary}i ) = $
      \n$ ( #{z.real} - #{w.real} ) + ( #{z.imaginary} - #{w.imaginary} )i = $
      \n$ \\textbf{Resultado = } #{z.real - w.real} + #{z.imaginary - w.imaginary}i $
      \n \\bigskip"
    end
  end

  def multiply(z, w, ex_id)
    TTY::File.append_to_file('Export/Trabalho.tex') do
      "\n\n\\textbf{#{ex_id})} $ #{z} * #{w} = $
      \n$ ( #{z.real} + #{z.imaginary} ) * ( #{w.real} + #{w.imaginary} ) = $
      \n$ #{z.real} * #{w.real} + #{z.real} * #{w.imaginary}i + #{w.real} * #{z.imaginary} + #{z.imaginary} * #{w.imaginary}i^{2} $
      \n\\textbf{Resultado = } $#{Complex(z.real, z.imaginary) * Complex(w.real, w.imaginary)} $
      \n \\bigskip"
    end
  end

  def divide(z, w, ex_id)
    TTY::File.append_to_file('Export/Trabalho.tex') do
      "\n\n\\textbf{#{ex_id})} $ \\frac{#{z}}{#{w}} = $
      \n$ \\frac{#{z}}{#{w}} = \\frac{#{z}}{#{w}} * \\frac{#{Complex(w).conjugate}}{#{Complex(w.conjugate)}} = \\textbf{#{Complex(z / w)}} $
      \n \\bigskip"
    end
  end

  def compile
    TTY::File.append_to_file('Export/Trabalho.tex', "\n\\end{document}")
    sleep(0.5)
    system("cd Export && pdflatex Trabalho.tex")
    sleep(0.1)
    system("clear")
  end

  def writeLatex
    TTY::File.append_to_file('Export/Trabalho.tex') do
      text = gets.chomp
      "#{text}"
    end
  end
end